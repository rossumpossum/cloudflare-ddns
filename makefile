.PHONY: docker-run, docker

.DEFAULT: docker-run

build-tag := dockerhub.valler.ca/rossumpossum/cloudflare-ddns

build:
	@echo "Building binary..."
	@GOOS=linux GOARCH=amd64 go build -o "bin/linux/cloudflare-ddns" .
	@docker build --build-arg UID=501 --build-arg GID=501 -t $(build-tag) .

docker-run:
	@make build
	@docker run --rm -it \
		-e CLOUDFLARE_TOKEN="YOUR-API-TOKEN" \
		-e CLOUDFLARE_EMAIL="someone@example.com" \
		-e CLOUDFLARE_DOMAIN="mydomain.com" \
		-e DDNS_RECORD="sub.mydomain.com" $(build-tag)

docker:
	@echo "Building binary..."
	@GOOS=linux GOARCH=amd64 go build -o "bin/linux/cloudflare-ddns" .
	@docker build --build-arg UID=1000 --build-arg GID=1001 -t $(build-tag) .
	@docker push $(build-tag)
