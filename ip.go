package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var (
	IP_URLS = []string{
		"http://v4.icanhazip.com",
		"http://canihazip.com/s",
		"http://ipecho.net/plain",
		"https://api.ipify.org",
		"http://myexternalip.com/raw",
	}
)

func IP() string {
	ip := make(chan string)

	//-----------------------------------------
	// Loop each cover URL and start a routine
	//-----------------------------------------
	for _, url := range IP_URLS {
		go getIP(url, ip)
	}

	//-----------------------------------------
	// Listen for a response from each and
	// return as soon as we get something
	//-----------------------------------------
	for range IP_URLS {
		if ipAddress := <-ip; ipAddress != "" {
			return ipAddress
		}
	}

	return ""
}

func getIP(url string, IPChannel chan string) {
	IPAddress := ""

	//-----------------------------------------
	// Make request
	//-----------------------------------------
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Error getting IP: %s\n", err.Error())
		IPChannel <- IPAddress
		return
	}
	defer resp.Body.Close()

	//-----------------------------------------
	// If not success, error
	//-----------------------------------------
	if resp.StatusCode != 200 {
		IPChannel <- IPAddress
		return
	}

	//-----------------------------------------
	// Read the whole body in
	//-----------------------------------------
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading body: %s\n", err.Error())
		IPChannel <- IPAddress
		return
	}

	//-----------------------------------------
	// Write it back to the channel
	//-----------------------------------------
	IPAddress = strings.Trim(string(body), " \n\r\t")
	IPChannel <- IPAddress
}
