package main

import (
	"fmt"
	"os"
	"strconv"
)

var (
	CLOUDFLARE_TOKEN  = ""
	CLOUDFLARE_EMAIL  = ""
	CLOUDFLARE_DOMAIN = ""

	DDNS_RECORD = ""

	UPDATE_INTERVAL = 30
)

func init() {
	fmt.Printf("Loading config from env...\n")

	if token := os.Getenv("CLOUDFLARE_TOKEN"); token != "" {
		CLOUDFLARE_TOKEN = token
	} else {
		fmt.Println("Missing cloudflare token")
		os.Exit(1)
	}

	if email := os.Getenv("CLOUDFLARE_EMAIL"); email != "" {
		CLOUDFLARE_EMAIL = email
	} else {
		fmt.Println("Missing cloudflare email")
		os.Exit(1)
	}

	if domain := os.Getenv("CLOUDFLARE_DOMAIN"); domain != "" {
		CLOUDFLARE_DOMAIN = domain
	} else {
		fmt.Println("Missing cloudflare domain")
		os.Exit(1)
	}

	if record := os.Getenv("DDNS_RECORD"); record != "" {
		DDNS_RECORD = record
	} else {
		fmt.Println("Missing DDNS record")
		os.Exit(1)
	}

	if interval, err := strconv.Atoi(os.Getenv("UPDATE_INTERVAL")); err == nil && interval > 5 {
		UPDATE_INTERVAL = interval
	}
}
