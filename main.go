package main

import (
	"log"
	"net/http"
	"time"

	"github.com/cloudflare/cloudflare-go"
	"github.com/labstack/echo"
	"github.com/rossvaller/ansi-terminal"
)

func main() {
	//-----------------------------------------
	// Check every x minutes
	//-----------------------------------------
	ticker := time.NewTicker(time.Duration(UPDATE_INTERVAL) * time.Minute)
	go func() {
		for {
			select {
			case <-ticker.C:
				//-----------------------------------------
				// Run our sync
				//-----------------------------------------
				sync()
			}
		}
	}()

	//-----------------------------------------
	// Sync now
	//-----------------------------------------
	go func() {
		time.Sleep(10 * time.Second)
		sync()
	}()

	// Spin up server to return the IP we have
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, IP())
	})
	e.Logger.Fatal(e.Start(":8080"))
}

func sync() {
	log.Printf("Syncing...\n")

	//-----------------------------------------
	// Get IP address
	//-----------------------------------------
	ip := IP()
	log.Printf("Got IP: %s\n", ip)

	//-----------------------------------------
	// Initialize the API
	//-----------------------------------------
	api, err := cloudflare.New(CLOUDFLARE_TOKEN, CLOUDFLARE_EMAIL)
	if err != nil {
		log.Fatal(err)
	}

	//-----------------------------------------
	// Get the zone ID by name
	//-----------------------------------------
	zoneID, err := api.ZoneIDByName(CLOUDFLARE_DOMAIN) // Assuming example.com exists in your CloudFlare account already
	if err != nil {
		log.Fatal(err)
	}

	//-----------------------------------------
	// Find the named record on the API
	//-----------------------------------------
	records, err := api.DNSRecords(zoneID, cloudflare.DNSRecord{
		Name: DDNS_RECORD,
	})
	if err != nil {
		log.Fatal(err)
	}

	//-----------------------------------------
	// Make sure we found the record
	//-----------------------------------------
	if len(records) != 1 {
		log.Fatal("DDNS record not found.")
	}

	//-----------------------------------------
	// Is IP the same?
	//-----------------------------------------
	DNS := records[0]
	if DNS.Content == ip {
		log.Println("IP Address is up-to-date.")
		return
	}

	//-----------------------------------------
	// IP is not the same, update
	//-----------------------------------------
	DNS.Content = ip
	if err := api.UpdateDNSRecord(zoneID, DNS.ID, DNS); err != nil {
		log.Printf("Couldn't update DNS record: %s\n", err.Error())
		return
	}

	log.Println(colors.Green("IP Address updated."))
}
