cloudflare-ddns
======

Docker-compatible Cloudflare dynamic DNS updater written in [Go](https://golang.org/).

Sources your current IP address from various services and uses whichever responds first to update a Cloudflare DNS record.

#### IP Address Services
 - http://icanhazip.com/
 - http://canihazip.com/s
 - http://ipecho.net/plain
 - https://api.ipify.org
 - http://myexternalip.com/raw

#### Environment Variables
| Key     | Description     | Example |
| -------- | -------- | -------- |
|`CLOUDFLARE_TOKEN`   | Cloudflare user API key   | ab90426aa5f73d95f3c718f437d7e0e2 |
|`CLOUDFLARE_EMAIL` | Cloudflare Email address | sign-in@cloudflare.com |
|`CLOUDFLARE_DOMAIN`   | The domain the DDNS record belongs to   | example.com |
|`DDNS_RECORD`  | Which record to update if the IP has changed   | myddns.example.com |
|`UPDATE_INTERVAL` _(optional)_  | How often to synchronize (in minutes)   | Defaults to **5 MINUTES** |

#### Running the container
```bash
$ docker run \
	-e CLOUDFLARE_TOKEN=YOUR_CLOUDFLARE_API_KEY \
	-e CLOUDFLARE_EMAIL=youremail@cloudflare.com \
	-e CLOUDFLARE_DOMAIN=example.com \
	-e DDNS_RECORD=myrecord.example.com \
	rossumpossum/cloudflare-ddns
```

#### Running the server directly
```bash
$ git clone git@gitlab.com:rossumpossum/cloudflare-ddns.git
$ cd cloudflare-ddns
$ go build -o cloudflare-ddns .
$ CLOUDFLARE_TOKEN=YOUR_CLOUDFLARE_API_KEY \
	CLOUDFLARE_EMAIL=youremail@cloudflare.com \
	CLOUDFLARE_DOMAIN=example.com \
	DDNS_RECORD=myrecord.example.com \
	./cloudflare-ddns
```