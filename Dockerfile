FROM ubuntu

RUN apt-get update && apt-get install -y ca-certificates

ARG UID
ARG GID

RUN groupadd -g ${GID} media && useradd -u ${UID} -g ${GID} cloudflare

CMD ["/cloudflare-ddns"]

ADD bin/linux/cloudflare-ddns /cloudflare-ddns
RUN chmod +x /cloudflare-ddns

USER cloudflare
